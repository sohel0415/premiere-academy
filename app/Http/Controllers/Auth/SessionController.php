<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\LoginValidation;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SessionController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout']]);
    }

    public function signin()
    {
        return view('auth.signin');
    }

    public function postSignin(LoginValidation $loginValidation)
    {

        $loginValidation->all();
        $attempt = Auth::attempt([
            'username'=>$loginValidation->get('username'),
            'password' => $loginValidation->get('password'),
            'isActive' => 1
        ]);

        if($attempt) return redirect()->intended('/dashboard');

        return redirect()->back()->with('errorMessage','Invalid Credentials!!');
    }
    public function logout()
    {
        Auth::logout();

        return redirect('signin');
    }
}
