<?php
/**
 * Created by PhpStorm.
 * User: sohel0415
 * Date: 8/18/2018
 * Time: 3:43 PM
 */

namespace App\Http\Controllers;


class DashboardController extends Controller{

    public function dashboard(){
        return view('dashboard');
    }
}