@extends('layout.layout')

@section('title')
    <title>Signin</title>
@endsection

@section('stylesheet')

@endsection

@section('content')
    <div class="page-content--bge5">
        <div class="container">
            <div class="login-wrap">
                <div class="login-content">
                    <div class="login-logo">
                        <a href="#">
                            <img src="{{asset('/resources/images/icon/logo.png')}}" alt="Premiere Academy">
                        </a>
                    </div>
                    <div class="login-form">
                        {{Form::open(array('url'=>'/signin'))}}
                            <div class="form-group">
                                <label>Username</label>
                                <input class="au-input au-input--full" type="text" name="username" placeholder="username" value="{{\Illuminate\Support\Facades\Input::old('username')}}" required>
                                <span class="validator_output <?php if($errors->first('username')!=null) echo "alert-danger"?>">{{ $errors->first('username') }}</span>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input class="au-input au-input--full" type="password" name="password" placeholder="Password">
                                <span class="validator_output <?php if($errors->first('password')!=null) echo "alert-danger"?>">{{ $errors->first('password') }}</span>
                            </div>
                            <div class="login-checkbox">
                                <label>
                                    <input type="checkbox" name="remember">Remember Me
                                </label>
                                <label>
                                    <a href="#">Forgotten Password?</a>
                                </label>
                            </div>
                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">sign in</button>

                        {{Form::close()}}
                        <div class="register-link">
                            <p>
                                Don't you have account?
                                <a href="{{url('/signup')}}">Sign Up Here</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

@endsection